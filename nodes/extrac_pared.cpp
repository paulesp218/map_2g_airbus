#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl/search/search.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>


using namespace  std;
static ros::Publisher pub;


static pcl::PointCloud<pcl::PointXYZ>::Ptr pared_xz (new pcl::PointCloud<pcl::PointXYZ>);
static pcl::PointCloud<pcl::PointXYZ>::Ptr pared_yz (new pcl::PointCloud<pcl::PointXYZ>);
static pcl::PointCloud<pcl::PointXYZ>::Ptr pared_filtro (new pcl::PointCloud<pcl::PointXYZ>);
static  pcl::PointCloud<pcl::PointXYZ>::Ptr output (new pcl::PointCloud<pcl::PointXYZ>);

void  cloud_xz (const sensor_msgs::PointCloud2ConstPtr& msg)
{

pcl::fromROSMsg(*msg,*pared_xz);
}

void  cloud_yz (const sensor_msgs::PointCloud2ConstPtr& msg)
{
 pcl::fromROSMsg(*msg,*pared_yz);



}

void  cloud_filter (const sensor_msgs::PointCloud2ConstPtr& msg)
{

     pcl::fromROSMsg(*msg,*pared_filtro);

     *output=*pared_xz+*pared_yz;
     *output+=*pared_filtro;

     ros::Time current_time;
     current_time = ros::Time::now();



   sensor_msgs::PointCloud2 cloud_publish;
   cloud_publish.header=msg->header;
   cloud_publish.header.stamp= current_time;
   pcl::toROSMsg(*output,cloud_publish);
   pub.publish(cloud_publish);
}

int main (int argc, char** argv)
{

    // Initialize ROS
    ros::init (argc, argv, "union_paredes");
    ros::NodeHandle nh;

    // Create a ROS subscriber for the input point cloud
    ros::Subscriber sub = nh.subscribe ("/wallxz", 1, cloud_xz);
    ros::Subscriber sub1 = nh.subscribe ("/wallyz", 1, cloud_yz);
    ros::Subscriber sub2 = nh.subscribe ("/passthrough/output", 1, cloud_filter);



    // Create a ROS publisher for the output point cloud
    pub = nh.advertise<sensor_msgs::PointCloud2> ("/wall_fusion", 1);

    // Spin
    ros::spin ();
}
