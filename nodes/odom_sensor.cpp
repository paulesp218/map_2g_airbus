#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <iostream>



#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

static ros::Publisher pub;
static ros::Publisher odom_pub;

using namespace std;
ros::Time current_time;
sensor_msgs::PointCloud2 output;

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{




  // Create a container for the data.




  // Do data processing here...
  output = *input;



}

void poseCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    static tf::TransformBroadcaster br3;
    static tf::Transform transform3;



    current_time = ros::Time::now();




    double x,y,z;
    double r,p,ya,w,vx,vy,vth;
    x=msg->pose.pose.position.x;
    y= msg->pose.pose.position.y;

    //z=msg->pose.pose.position.z;
    z=0;





    r=msg->pose.pose.orientation.x;
    p=msg->pose.pose.orientation.y;
    ya=msg->pose.pose.orientation.z;
    w=msg->pose.pose.orientation.w;


    //    r=0;
        //p=0;
       // ya= 0;
       // w=1;

    vx=msg->twist.twist.linear.x;
    vy=msg->twist.twist.linear.y;
    vth=msg->twist.twist.linear.z;







    //next, we'll publish the odometry message over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp = current_time;
    odom.header.frame_id = "odom";
    odom.pose.covariance=msg->pose.covariance;

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = z;
    odom.pose.pose.orientation.x=r;
    odom.pose.pose.orientation.y=p;
    odom.pose.pose.orientation.z=ya;
    odom.pose.pose.orientation.w=w;

    //set the velocity
    odom.header.frame_id = "odom";
    odom.child_frame_id = "base_link";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;
    odom.twist.covariance=msg->twist.covariance;

    //publish the message
    odom_pub.publish(odom);

    transform3.setOrigin( tf::Vector3(x,y,z));
    transform3.setRotation( tf::Quaternion(r,p,ya,w) );
    br3.sendTransform(tf::StampedTransform(transform3, current_time,"odom", "base_link" ));


    output.header.stamp=current_time;
    output.header.frame_id="velodyne_points";
    // Publish the data.
    pub.publish (output);


}

int main (int argc, char ** argv)
 {
  ros::init(argc, argv, "my_tf_broadcaster");

  ros::NodeHandle node;

  ros::Subscriber sub = node.subscribe("/drone6/odometry/filtered", 1, &poseCallback);

  odom_pub = node.advertise<nav_msgs::Odometry>("odom", 10);


  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub1 = node.subscribe ("/os1_node/points", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  pub = node.advertise<sensor_msgs::PointCloud2> ("/velodyne_cloud", 10);

  ros::spin();
  return 0;

};

