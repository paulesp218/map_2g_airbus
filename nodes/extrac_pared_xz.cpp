#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>
#include <pcl/search/search.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/filters/extract_indices.h>
using namespace  std;
static ros::Publisher pub;
static ros::Publisher pub1;
static std::vector<ros::Publisher> pub_vec;
static sensor_msgs::PointCloud2::Ptr downsampled, output;


void  cloud_cb (const sensor_msgs::PointCloud2ConstPtr& msg)
{
    // Convert to pcl point cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(*msg,*cloud);
    //ROS_DEBUG("%s: new ponitcloud (%i,%i)(%zu)",_name.c_str(),cloud_msg->width,cloud_msg->height,cloud_msg->size());


    // Get segmentation ready
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_PARALLEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.5);
    seg.setMaxIterations (300);
    seg.setAxis(Eigen::Vector3f(1,0,0));
    seg.setAxis(Eigen::Vector3f(0,0,1));
    seg.setEpsAngle (2.35);





    // Create pointcloud to publish inliers
    pcl::PointCloud<pcl::PointXYZ>::Ptr clustered_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane_antes (new pcl::PointCloud<pcl::PointXYZ>);
    int original_size(cloud->points.size ());
    int n_planes(0);

    while (cloud->points.size ()>original_size*0.1){
    //for (n_planes;n_planes<2;n_planes++) {

        // Fit a plane
        seg.setInputCloud(cloud);
        seg.segment(*inliers, *coefficients);

        if (inliers->indices.size () == 0)
             {
                 std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
                 break;
             }
        // Extract planer inliers nad surface
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_rest (new pcl::PointCloud<pcl::PointXYZ>);

        extract.setInputCloud(cloud);
        extract.setIndices(inliers);
        extract.setNegative(false);
        extract.filter(*cloud_plane);
        cloud_plane->header.frame_id=msg->header.frame_id;

        // Remove the planar inliers, extract the rest
        extract.setNegative (true);
        extract.filter (*cloud_rest);
        *cloud = *cloud_rest;

        pcl::PointXYZ minPt, maxPt;
        pcl::getMinMax3D (*cloud_plane, minPt, maxPt);

        float zmin,zmax,ymin,ymax,xmin,xmax,z,x,y,puntos;
        zmin=minPt.z;
        zmax=maxPt.z;
        z=zmax-zmin;

        xmin=minPt.x;
        xmax=maxPt.x;
        x=fabs(xmax-xmin);

        ymin=minPt.y;
        ymax=maxPt.y;
        y=fabs(ymax-ymin);
        puntos=cloud_plane->points.size();

        if((1<y & y<100) & (x<2))
        {

            *clustered_cloud+=*cloud_plane;
            clustered_cloud->header.frame_id=cloud_plane->header.frame_id;

        }


//                    clustered_cloud->header.frame_id=cloud_plane->header.frame_id;
//                    *clustered_cloud=*cloud_plane;

        n_planes++;
        cout<<"plano:"<<n_planes<<" puntos: "<<cloud_plane->points.size()<<endl;
        cout<<x<<" "<<y<<" "<<z<<endl;


    }

    cout<<"NUEVO NUEVE"<<endl;
    // Publish points
    sensor_msgs::PointCloud2 cloud_publish;
    cloud_publish.header=msg->header;
    pcl::toROSMsg(*clustered_cloud,cloud_publish);
    pub.publish(cloud_publish);




}





int main (int argc, char** argv)
{

    // Initialize ROS
    ros::init (argc, argv, "extraccion_xz");
    ros::NodeHandle nh;

    // Create a ROS subscriber for the input point cloud
    ros::Subscriber sub = nh.subscribe ("/velodyne_cloud", 1, cloud_cb);


    // Create a ROS publisher for the output point cloud
    pub = nh.advertise<sensor_msgs::PointCloud2> ("/wallxz", 1);

    // Spin
    ros::spin ();
}
