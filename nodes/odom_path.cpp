
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include <gazebo_msgs/ModelStates.h>
#include "std_msgs/String.h"
#include <iostream>
#include <fstream>
#include <map_2g_airbus/amcl_msg.h>
using namespace std;


static double x_odom,y_odom;
static ros::Publisher odom_pub;
static ros:: Publisher amcl_pub;
static float segundos;
static int it;
static nav_msgs::Path path;
static double xa, ya,x_amcl,y_amcl;
static bool temp;


void amcl_data(const geometry_msgs::PoseWithCovarianceStampedConstPtr& msg)
{

it++;
     temp=true;
     x_amcl = msg->pose.pose.position.x;
     y_amcl = msg->pose.pose.position.y;



    double x,y,xy;
   x = msg->pose.covariance[0]; 
   xy = abs(msg->pose.covariance[1]); 
   y = msg->pose.covariance[7];

   map_2g_airbus::amcl_msg msge;


   msge.posx=x_amcl;
   msge.posy=y_amcl;
   
  msge.covx=abs(x);
  msge.covy =abs(y);


if(xy<=0.0537)
{
msge.converge=1;

}
else
msge.converge=0;

msge.ite=it;
msge.ts=float(segundos/10);

 amcl_pub.publish(msge);


//     static geometry_msgs::PoseStamped pose1;
//    ros::Time current_time;
//    current_time = ros::Time::now();

//    pose1.header.stamp =current_time;
//    pose1.header.frame_id=msg->header.frame_id;
//    pose1.pose=msg->pose.pose;

//    path1.header.stamp=current_time;
//    path1.header.frame_id=msg->header.frame_id;
//    path1.poses.push_back(pose1);




//    if(compare_position(x_amcl,y_amcl,x1a,y1a) && !( msg->header.frame_id.empty()))
//     amcl_pub.publish(path1);

//     x1a=x_amcl;
//     y1a=y_amcl;
     //f3=true;




    


}


void callback1(const ros::TimerEvent&)
{
if(temp)
 segundos++;
}



bool compare_position( double x, double y , double x_a, double y_a)
{
    double o1;
    o1=abs(x-x_a);

    double o2;
    o2=abs(y-y_a);

    if(o1>=0.02 && o2>=0.02)
        return true;
}

void odom_data(const nav_msgs::Odometry::ConstPtr& msg)
{

    static geometry_msgs::PoseStamped pose;

    x_odom=msg->pose.pose.position.x;
    y_odom= msg->pose.pose.position.y;

    ros::Time current_time;
    current_time = ros::Time::now();


    pose.header.stamp =current_time;
    pose.header.frame_id=msg->header.frame_id;
    pose.pose=msg->pose.pose;

    path.header.stamp=current_time;
    path.header.frame_id=msg->header.frame_id;
    path.poses.push_back(pose);



    if(compare_position(x_odom,y_odom,xa,ya))
        odom_pub.publish(path);

    xa=x_odom;
    ya=y_odom;

}


int main (int argc, char ** argv)
{

    ros::init(argc, argv, "odom_path");
    ros::NodeHandle node;
    //ros::Subscriber sub = node.subscribe("/odom", 1, &odom_data);
    //odom_pub = node.advertise<nav_msgs::Path>("odom_path", 10);

    ros::Subscriber sub1 = node.subscribe("/amcl_pose", 1, &amcl_data);
    amcl_pub = node.advertise<map_2g_airbus::amcl_msg>("amcl_info", 1);
    ros::Timer timer1 = node.createTimer(ros::Duration(0.1), callback1);


    ros::spin();

    return 0;

};
