#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/LaserScan.h>



using namespace  std;

ros::Publisher pub1;
static sensor_msgs::LaserScan scan_filter;
static sensor_msgs::LaserScan scan_wfusion;
static sensor_msgs::LaserScan scan_join;
static bool ok, ok1;
static float scan_antes;

void cloud_cb_filter (const sensor_msgs::LaserScanConstPtr& scan_msg)
{
    scan_filter=*scan_msg;
    ok=true;
   // cout<<scan_msg->ranges.size()<<endl;
}



void cloud_cb (const sensor_msgs::LaserScanConstPtr& scan_msg)
{
    //cout<<"2: "<<scan_msg->ranges.size()<<endl;
    
    

    if(ok)
    {
        sensor_msgs::LaserScan scan_out;

        scan_out=*scan_msg;


        for(int i=0;i<scan_msg->ranges.size ();i++)
        {


            if(fabs(scan_msg->ranges[i]-scan_filter.ranges[i])<5)
            {
                scan_out.ranges[i]=0;

            }
            else
                scan_out.ranges[i]=scan_msg->ranges[i];


        }

        pub1.publish(scan_out);
        ok=false;


    }




}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "my_pcl");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/front/scan/filter", 1, cloud_cb_filter);
  ros::Subscriber sub2 = nh.subscribe ("/front/scan", 1, cloud_cb);
  // Create a ROS publisher for the output point cloud
  pub1 = nh.advertise<sensor_msgs::LaserScan> ("/scan_out", 1);



  // Spin
  ros::spin ();
}
